#include <iostream>

using namespace std;

void swap(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
}

void selection2Sort (int A[], int n, char smjer){
    int j;
    int min, max;
    for(int i = 0; i < n; i++){
        min = i;
        max = i;
        for(j = i + 1; j < n; j++){
            if (A[j] < A[min]) min = j;
            if (A[j] > A[max]) max = j;
        }
        if(smjer == '0') swap(A[i], A[min]);
        else swap(A[max], A[i]);

    }
    return;

}

int main(){

    int A[] = {1,3,2,6,5};

    selection2Sort(A, 5, '1');
    
    for(int i =0; i < 5; i++) cout << A[i] << " ";


    return 0;
}