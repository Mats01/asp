#include<iostream>
using namespace std;

string merge(string A, string B){
    int aInd = 0, bInd = 0;
    string result;
    while(A[aInd] != '\0' && B[bInd] != '\0'){
        if(A[aInd] > B[bInd]){
             result += A[aInd++];
        } else{
            result += B[bInd++];
        }
    }
    
    while(A[aInd] != '\0'){
        result += A[aInd++];
    }
    while(B[bInd] != '\0'){
        result += B[bInd++];
    }
    return result;
}
int main(){
    string a = "ecaDB", b = "dbECA";
    cout << merge(a,b);
    int i = 0;
    cout << "\n" << a[++i];
    return 0;
}