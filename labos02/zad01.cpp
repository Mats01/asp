#include <iostream>

using namespace std;

typedef struct {
    int postanskiBroj;
    string mjesto;
} Zapis;


void insertionSort (Zapis A[], int n, char smjer){
    size_t i, j;
    for(i = 1; i < n; i++){
        Zapis temp = A[i];
        for(j = i; j >= 1 && ((smjer == '0'&& A[j-1].postanskiBroj > temp.postanskiBroj) || (smjer == '1'&& A[j-1].postanskiBroj < temp.postanskiBroj)); j--){
            A[j] = A[j-1];

        }
        A[j] = temp;

    }

    return;
}

int main(){


    Zapis z1, z2;
    z1.postanskiBroj = 1;
    z1.mjesto = "pula";
    z2.postanskiBroj = 2;
    z2.mjesto = "zg";

    Zapis lis[2];
    lis[0] = z2;
    lis[1] = z1;

    insertionSort(lis, 2, '0');



    return 0;
}