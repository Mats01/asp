#include <iostream>
#include <memory>

using namespace std;

class Stog{
    private:
        struct ElementStoga
        {
            int broj;
            shared_ptr<ElementStoga> slijedeci;

            ~ElementStoga(){
                cout << endl << "otio sam " << this->broj << endl;
            }

            
        };

        shared_ptr<ElementStoga> vrh = nullptr;

    public:
        bool dodaj(int broj){
            shared_ptr<ElementStoga> noviElement = make_shared<ElementStoga>();

            if(noviElement == nullptr){ printf("this happended"); return false; }

            noviElement->broj = broj;
            noviElement->slijedeci = vrh;
            vrh = noviElement;

            return true;
        }

        bool skini(int &broj){
            if (vrh == nullptr){
                return false;
            }

            broj = vrh->broj;

            vrh = vrh->slijedeci;

            return true;
        }


        

};


int main(){

    Stog s;
    for(int i = 0; i < 101; i++){
        s.dodaj(rand() % 100);
        
    }
    srand (time(NULL));
    int a;
    s.skini(a);
    cout << a;
    // for(int i = 0; i < 101; i++){
    //     s.skini(a);
    //     cout << a;
        
    // }
    

    


    return 0;
}