#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

using namespace std;

class Stog{
    private:
        static const int MAX = 10;

        int stack[MAX];

        int vrh = -1;

    public:
        bool dodaj(int broj){
            if(vrh >= MAX){
                return false;
            }

            stack[++vrh] = broj;

            return true;
        }

        bool skini(int &broj){
            if(vrh < 0){
                return false;
            }

            broj = stack[vrh--];
            return true;

        }

};


int main(){

    Stog s;
    for(auto i = 0; i < 10; i++){
        auto a = rand() % 10 + 1;
        
        s.dodaj(a);
    }

    Stog pomocni_s;
    for(auto i = 0; i < 10; i++){
        int mrs_u_picku_materinu;
        s.skini(mrs_u_picku_materinu);
        cout << mrs_u_picku_materinu << " ";

        pomocni_s.dodaj(mrs_u_picku_materinu);

    }
    cout<<endl;
    for(auto i = 0; i < 10; i++){
        int mrs_u_picku_materinu;
        pomocni_s.skini(mrs_u_picku_materinu);
        cout << mrs_u_picku_materinu << " ";
        
    }
    


    return 0;
}