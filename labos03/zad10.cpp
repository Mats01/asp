/*
Osmisliti razred MaxGomila koji u strukturu podataka tipa gomila pohranjuje objekte tipa
string. Razred MaxGomila mora sadržavati metodu za upis elementa u gomilu (jedan po jedan)
(maxgomila – najveći element je u vrhu gomile) i metoda koja vraća element na vrhu gomile
(ali ga ne miče s gomile, kao STL metoda top).
Napisati glavni program u kojem se 5 elemenata upisuje s tipkovnice i ubacuje u gomilu te
ispisuje trenutno najveći element u gomili.
*/

#include<iostream>
#include<cmath>

using namespace std;


class MaxGomila {
    private:
        int *h;
        size_t n, k = 0;

        void uredi(){
            int j, i;
            int novi;

            j = this->k;
            i = j/2;
            novi = this->h[j];
            while (i > 0 && this->h[i] < novi){
                    this->h[j] = this->h[i];
                    j = i;
                    i = i/2;
            }
            this->h[j] = novi;
        }
        

    public:

        MaxGomila(int _n){
            h = new int[_n];
            this->n = _n;

        }
        void dodaj(int &broj){
            
            if(this->k >= n-1){
                cout << "gomila je puna!";
                return;

            }
            this->k++;
            this->h[k] = broj;
            this->uredi();


        }

        void Print() {
            size_t i = 1;
            size_t k = 1;
            while (i < this->n) { // loops to the last element of the heap
                // prints until the maximum element in the heap at level k
                for (; i <= pow(2, k) - 1 && i < this->n; i++) {
                    cout << " " << this->h[i] << " ";
                }
                k++; // go to the next level
                cout << "\n";
            }
        }




};


int main(){

    MaxGomila mg = MaxGomila(8);
    auto n = 7;
    int a[] = {8,5,7,3,2,1,4};
    for(auto i = 0; i <n; i++){
        mg.dodaj(a[i]);
    }
 

    mg.Print();




    return 0;
}