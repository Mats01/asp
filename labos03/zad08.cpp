#include <iostream>
#include <memory>

using namespace std;

/*
Osmisliti razred BinarnoStablo koje pohranjuje stringove u strukturu podataka binarno
stablo. Razred treba sadržavati funkcijske članove (metode) za upis elementa u uređeno
binarno stablo (lijevo abecedno „manji“, desno „veći“, a za jednake elemente ne dozvoliti upis),
traženje abecedno najvećeg elementa i ispis preorder. Napisati glavni program u kojem se 10
elemenata iz predefiniranog polja stringova ubacuje u stablo, poziva funkcija za traženje
najvećeg elementa te ga ispisati, a zatim ispisati elemente stabla u preorder poretku.
*/

template <typename T> struct Cvor
{
    T podatak;
    shared_ptr<Cvor<T>> lijevi;
    shared_ptr<Cvor<T>> desni;

    Cvor(const T &noviPodatak){
        this->podatak = noviPodatak;
    }
};



template <typename T> class BinarnoStablo{
    public:
        shared_ptr<Cvor<T>> korijen;
        BinarnoStablo(){
            korijen = nullptr;
        }

        bool jePrazno() const {
            return (this->korijen == nullptr);
        }

        void ubaci(const T &noviBroj){
            ubaci(this->korijen, noviBroj);
        }

        void ubaci(shared_ptr<Cvor<T>> &cvor, const T &noviPodatak){
            if(cvor){
                if(cvor->podatak < noviPodatak){
                    ubaci(cvor->desni, noviPodatak);
                } else if(cvor->podatak > noviPodatak){
                    ubaci(cvor->lijevi, noviPodatak);
                } else {
                    cout << "vec to imamo \n";
                }
            } else {
                cvor = make_shared<Cvor<T>>(noviPodatak);
            }

        }


        
        
};

int main(){

    BinarnoStablo<int> bs;
    bs.ubaci(2);



    return 0;
}