#include <iostream>

using namespace std;

class Red {
    private:
        static const int MAX = 10;
        float queue[MAX];
        int ulaz = 0;
        int izlaz = 0;
    
    public:
        bool dodaj (double broj){
            if((ulaz + 1) % MAX == izlaz){ // nema vise mista
                return false;
            } else {
                queue[ulaz] = broj;
                ulaz = (ulaz + 1) % MAX;
                return true;
            }           
        }

        bool skini (double *broj){
            if(ulaz == izlaz){ // prazan q
                return false;
            }

            *broj = queue[izlaz];
            izlaz = (izlaz + 1) % MAX;
            return true;
        }
};


int main(){
    Red r;

    r.dodaj(1);
    r.dodaj(2);
    r.dodaj(3);
    double a;
    r.skini(&a);
    cout << a;




    return 0;
}