#include <iostream>

using namespace std;

class Red{
    private:
        struct Cvor
        {
            double broj;
            Cvor *slijedeci;
        };

        Cvor *ulaz = nullptr;
        Cvor *izlaz = nullptr;

    public:
        bool dodaj(double broj){
            Cvor *noviBroj = new (nothrow) Cvor;
            if(noviBroj == nullptr) return false;

            noviBroj->slijedeci = nullptr;
            noviBroj->broj = broj;
            if(ulaz == nullptr){
                izlaz = noviBroj;
            } else {
                ulaz->slijedeci = noviBroj;
            }

            ulaz = noviBroj;
            return true;
        }

        bool skini(double *broj){
            if(izlaz == nullptr) return false;

            *broj = izlaz->broj;

            izlaz = izlaz->slijedeci;

            if(izlaz == nullptr) ulaz = nullptr;

            return true;
        }
        

};




int main(){

    Red r;

    r.dodaj(1);
    r.dodaj(2);
    r.dodaj(3);
    double a;
    r.skini(&a);
    cout << a;

    return 0;
}