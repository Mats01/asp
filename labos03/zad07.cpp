#include <iostream>
#include <memory>

using namespace std; 

/*
Osmisliti razred BinarnoStablo koje pohranjuje cijele brojeve u strukturu podataka binarno
stablo. Razred treba sadržavati funkcijske članove (metode) za upis elementa u uređeno
binarno stablo (lijevo manji, desno veći, a za jednake elemente ne dozvoliti upis), traženje
najmanjeg elementa i ispis inorder. Napisati glavni program u kojem se učitava 10 elemenata
i ubacuje u stablo, poziva funkcija traži i ispisuju elementi stabla inorder.
*/

struct Cvor{
    int broj;
    shared_ptr<Cvor> lijevi;
    shared_ptr<Cvor> desni;

    Cvor(const int &noviBroj) : broj(noviBroj), lijevi(nullptr), desni(nullptr) {}

    ostream& operator<< (Cvor& obj) {
		return cout << obj.broj;
	}


};

class BinarnoStablo{
    public:
        shared_ptr<Cvor> korijen;
        BinarnoStablo(){
            korijen = nullptr;
        }

        bool jePrazno() const {
            return (this->korijen == nullptr);
        }

        void ubaci(const int &noviBroj){
            ubaci(this->korijen, noviBroj);
        }

        void ubaci(shared_ptr<Cvor> &cvor, const int &noviBroj){
            if(cvor){
                if(cvor->broj < noviBroj){
                    ubaci(cvor->desni, noviBroj);
                } else if(cvor->broj > noviBroj){
                    ubaci(cvor->lijevi, noviBroj);

                } else{
                 throw std::invalid_argument("opa fuck " + std::to_string(noviBroj) + " vec imamo");


                }

            }
            else {
                cvor = make_shared<Cvor>(noviBroj);
            }

        }
        bool nadiNajmanji(){
            return nadiNajmanji(korijen);
        }

        bool nadiNajmanji(shared_ptr<Cvor> &cvor){
            if(cvor){
                if(cvor->lijevi){
                    nadiNajmanji(cvor->lijevi);
                } else{
                    cout << cvor->broj << " je najmanji broj \n";
                    return true;

                }

            }
            else
            {
                return false;
            }
            
        }

        void inorder(){
            return inorder(korijen);
        }

        void inorder(shared_ptr<Cvor> &cvor){
            if(cvor){
                if(cvor->lijevi){
                    inorder(cvor->lijevi);
                }
                cout << cvor->broj << " ";
                if(cvor->desni){
                    return inorder(cvor->desni);
                }
            }

        }




};


int main(){
    BinarnoStablo bs = BinarnoStablo();

    int N;
    cout << "koliko brojeva? \n";

    cin >> N;

    int broj;

    for(auto i = N; i >0; i--){
        cout << "ubaci broj u stablo \n";
        

        cin >> broj;

        bs.ubaci(broj);
    }


    bs.nadiNajmanji();

    bs.inorder();

  


    


    return 0;
}