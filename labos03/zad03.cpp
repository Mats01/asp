#include <iostream>
#include <memory>


using namespace std;

class Red{
    private:
        struct Cvor
        {
            double broj;
            shared_ptr<Cvor> slijedeci;

            
            ~Cvor(){
                cout << endl << "otio sam " << this->broj << endl;
            }
        };

        shared_ptr<Cvor> ulaz = nullptr;
        shared_ptr<Cvor> izlaz = nullptr;

       

    public:
        bool dodaj(double broj){
            shared_ptr<Cvor> noviBroj = make_shared<Cvor>();
            if(noviBroj == nullptr) return false;

            // noviBroj->slijedeci = make_shared<Cvor>();
            noviBroj->broj = broj;
            if(ulaz == nullptr){
                izlaz = noviBroj;
            } else {
                ulaz->slijedeci = noviBroj;
            }

            ulaz = noviBroj;
            return true;
        }

        bool skini(double *broj){
            if(izlaz == nullptr) return false;

            *broj = izlaz->broj;

            // Cvor *temp = izlaz;

            izlaz = izlaz->slijedeci;

            if(izlaz == nullptr) ulaz = nullptr;

            // delete temp;

            return true;
        }

        bool poljeURed (int polje[], int n){
            while (n > 0){
                if(!this->dodaj(polje[--n])) return false;
            }
            return true;
        }
        

};




int main(){

    Red r;

    int polje[] = {1,2,3,4,5};
    r.poljeURed(polje, 5);
    double a;
    r.skini(&a);
    cout << a;
    r.skini(&a);
    cout << a;
    r.skini(&a);
    cout << a;
    r.skini(&a);
    cout << a;
    r.skini(&a);
    cout << a;

    return 0;
}