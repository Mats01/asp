#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

using namespace std;

class Stog{
    private:
        static const int MAX = 100;

        int stack[MAX];

        int vrh = -1;

    public:
        bool dodaj(int broj){
            if(vrh >= MAX){
                return false;
            }

            stack[++vrh] = broj;

            return true;
        }

        bool skini(int &broj){
            if(vrh < 0){
                return false;
            }

            broj = stack[vrh--];
            return true;

        }

};


int main(){
    Stog s;
    for(int i = 0; i < 101; i++){
        s.dodaj(rand() % 100);
        
    }
    srand (time(NULL));
    int a;
    s.skini(a);
    cout << a;



    return 0;
}