#include <iostream>
#include <cmath>

using namespace std;

double f (double z, int k){
    // (-1)^k * z^(2k+1) / (2k + 1)!

   if (k == 0){
       return z;
   }

   return (-1.0 * pow(z,2)) / ((2*k + 1) * (2*k)) * f(z, k-1); 
    
}

int main(void){
    
    cout << f(0.5,3);

    return 0;
}