#include <iostream>
#include <cmath>

using namespace std;

void f (int polje[], int n, int m){
    if(n == 1){
        polje[0] = 1;
    }
    else {
        polje[n-1] = pow(m,n-1);
        f(polje, n-1, m);

    }
}

int main(void){
    int m,n;

    cout << "input m, n\n";

    cin >> m >> n;

    int* polje = new int[n];

    f(polje, n, m);

    printf("[");

    for(int i = 0; i < n; i++){
        
        if(i == n-1){
            printf("%d]", polje[i]);

        }else {
            printf("%d, ", polje[i]);
        }
        
    }
    
    return 0;
}