#include <iostream>
#include <regex>

using namespace std;

class SanitizedString{
    public:
        string sanitized;

        SanitizedString(string input){
            input = this->removeDuplicateWhitespace(input);
            sanitized = this->removeNonAplhaChars(input);
        }
        
        friend ostream &operator<<(ostream &os, const SanitizedString &s ) {
            os << s.sanitized;
            return os;
        };

   private:
    string removeDuplicateWhitespace(string input){
        regex r("([ ]+)");

        string result = regex_replace(input,r," ");

        return result;

    }
    string removeNonAplhaChars(string input){
        regex r("([^a-zA-z ])");

        string result = regex_replace(input,r,"");


        return result;
    }

};

int main(void){

    string input;
    cout << "input some input al nemojte zapocet s brojkom\n";
    // cin >> input;
    SanitizedString a("inpu2t   rere45");
    cout << a;
    

    return 0;
}